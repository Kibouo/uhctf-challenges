#! /usr/local/bin/python

import os
import time
from dotenv import load_dotenv
from picotui.context import Context
from picotui.screen import Screen
from picotui.widgets import *
from picotui.defs import *

load_dotenv()

WANTED_WIDTH = 80
WANTED_HEIGHT = 24
announcement_height = 3
STATUS_FRAME = {
    'width': 54,
    'height': WANTED_HEIGHT - 2 - announcement_height,
    'x': 1,
    'y': announcement_height + 1
}
ANNOUNCEMENT_FRAME = {
    'width': STATUS_FRAME['width'],
    'height': announcement_height,
    'x': 1,
    'y': 1
}
keycard_frame_height = 3
CONTROL_FRAME = {
    'width': WANTED_WIDTH - (STATUS_FRAME['width'] + STATUS_FRAME['x']),
    'height': WANTED_HEIGHT - 2 - keycard_frame_height,
    'x': STATUS_FRAME['width'] + STATUS_FRAME['x'] + 1,
    'y': 1
}
KEYCARD_FRAME = {
    'width': WANTED_WIDTH - (STATUS_FRAME['width'] + STATUS_FRAME['x']),
    'height': keycard_frame_height,
    'x': STATUS_FRAME['width'] + STATUS_FRAME['x'] + 1,
    'y': CONTROL_FRAME['height'] + CONTROL_FRAME['y'],
}


class State():
    def __init__(self):
        # input
        self.key_card = False
        self.force_open_gate = False
        self.force_launch = False
        self.override_blockage = False
        self.launch_request = False

        # output
        self.manual_mode = False
        self.auto_mode = False
        self.launch_tube_blocked = True
        self.gate = False
        self.mech_ready = False
        self.fire_launchpad = False

        # state transitions
        self.__fire_launchpad_positive_edge = False

    def scan_state(self):
        # stop changes when solved
        if self.solved():
            return
        self.__scan_mode()
        self.__scan_override()
        self.__scan_gate()
        self.__scan_launchpad()

        self.__reset_state_transitions()

    def __scan_mode(self):
        self.manual_mode = self.key_card
        self.auto_mode = not self.manual_mode

    def __scan_override(self):
        if self.manual_mode and self.override_blockage:
            self.launch_tube_blocked = False

    def __scan_gate(self):
        self.gate = not self.launch_tube_blocked and (
            (self.manual_mode and self.force_open_gate) or
            (self.gate) or
            (self.mech_ready and self.auto_mode and self.launch_request))

    def __scan_launchpad(self):
        prev_fire_launchpad_state = self.fire_launchpad
        would_be_fire_launchpad_state = self.gate and ((self.manual_mode and self.force_launch) or (
            self.auto_mode and self.mech_ready and self.launch_request))
        # only if it goes positive do we launch
        self.fire_launchpad = not prev_fire_launchpad_state and would_be_fire_launchpad_state

        self.__fire_launchpad_positive_edge = not prev_fire_launchpad_state and would_be_fire_launchpad_state

        if self.__fire_launchpad_positive_edge:
            self.mech_ready = False

    def __reset_state_transitions(self):
        self.__fire_launchpad_positive_edge = False

    def solved(self):
        return self.fire_launchpad


state = State()
dialog = None
boot_start_time = None
update_ui_fn = None


def boot_mech():
    global state
    global boot_start_time
    if not state.launch_tube_blocked and not state.mech_ready and boot_start_time is None:
        boot_start_time = int(time.time())
    elif not state.launch_tube_blocked and not state.mech_ready:
        if int(time.time()) - boot_start_time > 4:
            state.mech_ready = True
            boot_start_time = None


def screen_resize(screen):
    global dialog
    dialog = create_dialog()
    screen_redraw(screen)


def screen_redraw(screen, allow_cursor=False):
    global dialog
    screen.attr_color(C_WHITE, None)
    screen.cls()
    screen.attr_reset()
    dialog.redraw()


class KeyCardButton(WButton):
    def __init__(self, w, state=False):
        self.state = state
        super().__init__(w, 'v remove v' if self.state else '^ insert ^')

    def redraw(self):
        self.t = 'v remove v' if self.state else '^ insert ^'

        self.goto(self.x, self.y)
        secondary_color = C_YELLOW if self.state else C_BLUE

        if self.disabled:
            self.attr_color(C_WHITE, C_GRAY)
        else:
            if self.focus:
                self.attr_color(C_BLACK, secondary_color)
            else:
                self.attr_color(C_BLACK, secondary_color)
        self.wr(self.t.center(self.w))
        self.attr_reset()


class IOButton(WButton):
    def __init__(self, w, state=False):
        self.state = state
        super().__init__(w, 'I' if self.state else 'O')

    def redraw(self):
        self.t = 'I' if self.state else 'O'

        self.goto(self.x, self.y)
        secondary_color = C_GREEN if self.state else C_RED

        if self.disabled:
            self.attr_color(C_WHITE, C_BLACK)
        else:
            if self.focus:
                self.attr_color(C_B_WHITE, secondary_color)
            else:
                self.attr_color(C_BLACK, secondary_color)
        self.wr(self.t.center(self.w))
        self.attr_reset()


class AnnounceLabel(WButton):
    # Based on WButton to add color to text
    def __init__(self, w=0):
        self.disabled = True
        super().__init__(w, '')

    def __set_text(self):
        global state
        if state.solved():
            self.t = os.getenv('FLAG')
        elif not state.launch_tube_blocked and not state.mech_ready:
            self.t = '/!\\ BOOTING MECH /!\\'
        elif not state.launch_tube_blocked and state.mech_ready:
            self.t = ''
        else:
            self.t = '/!\\ DANGER /!\\'

    def __set_color(self):
        global state
        if state.solved():
            self.attr_color(C_GREEN, None)
        elif not state.launch_tube_blocked and not state.mech_ready:
            self.attr_color(C_YELLOW, None)
        else:
            self.attr_color(C_RED, None)

    def redraw(self):
        self.goto(self.x, self.y)
        self.__set_text()
        self.__set_color()
        self.wr(self.t.center(self.w))
        self.attr_reset()


def create_dialog():
    global state
    width, height = Screen.screen_size()
    dialog = Dialog((width - WANTED_WIDTH) // 2, (height -
                    WANTED_HEIGHT) // 2, WANTED_WIDTH, WANTED_HEIGHT)

    # frames
    dialog.add(ANNOUNCEMENT_FRAME['x'], ANNOUNCEMENT_FRAME['y'], WFrame(
        ANNOUNCEMENT_FRAME['width'], ANNOUNCEMENT_FRAME['height'], ""))
    dialog.add(STATUS_FRAME['x'], STATUS_FRAME['y'], WFrame(
        STATUS_FRAME['width'], STATUS_FRAME['height'], "Status"))
    dialog.add(CONTROL_FRAME['x'], CONTROL_FRAME['y'],
               WFrame(CONTROL_FRAME['width'], CONTROL_FRAME['height'], "Control"))
    dialog.add(KEYCARD_FRAME['x'], KEYCARD_FRAME['y'], WFrame(
        KEYCARD_FRAME['width'], KEYCARD_FRAME['height'],  "Key card"))

    # announcement
    announcement_label = AnnounceLabel(ANNOUNCEMENT_FRAME['width'] - 4)
    dialog.add(ANNOUNCEMENT_FRAME['x'] + 2, ANNOUNCEMENT_FRAME['y'] +
               1, announcement_label)

    # status
    # TODO: expand with random fun stuff? Only for show. Random graphs maybe? (fuel, pilot status, temperature, etc)
    operation_label = WLabel('', STATUS_FRAME['width'] - 4)
    dialog.add(STATUS_FRAME['x'] + 2, STATUS_FRAME['y'] +
               1, operation_label)
    launch_label = WLabel('', STATUS_FRAME['width'] - 4)
    dialog.add(STATUS_FRAME['x'] + 2, STATUS_FRAME['y'] +
               2, launch_label)
    mech_label = WLabel('', STATUS_FRAME['width'] - 4)
    dialog.add(STATUS_FRAME['x'] + 2, STATUS_FRAME['y'] +
               3, mech_label)
    gate_label = WLabel('', STATUS_FRAME['width'] - 4)
    dialog.add(STATUS_FRAME['x'] + 2,
               STATUS_FRAME['y'] + 4, gate_label)
    launchpad_label = WLabel('', STATUS_FRAME['width'] - 4)
    dialog.add(STATUS_FRAME['x'] + 2,
               STATUS_FRAME['y'] + 5, launchpad_label)

    def update_operation_label():
        operation_label.t = f"Operation mode: {'manual' if state.manual_mode else 'auto'}"
        operation_label.redraw()

    def update_launch_label():
        launch_label.t = f"Launch tube:    {'blocked' if state.launch_tube_blocked else 'clear'}"
        launch_label.redraw()

    def update_mech_label():
        mech_label.t = f"Mech status:    {'standby' if state.mech_ready else 'booting'}"
        mech_label.redraw()

    def update_gate_label():
        gate_label.t = f"Gate:           {'open' if state.gate else 'closed'}"
        gate_label.redraw()

    def update_launchpad_label():
        launchpad_label.t = f"Launchpad:      {'firing' if state.fire_launchpad else 'ready'}"
        launchpad_label.redraw()

    # controls
    # TODO: expand with random fun stuff? Only for show.
    launch_request_button = IOButton(3)
    dialog.add(CONTROL_FRAME['x'] + 2,
               CONTROL_FRAME['y'] + 1, launch_request_button)
    dialog.add(CONTROL_FRAME['x'] + 6,
               CONTROL_FRAME['y'] + 1, WLabel('Launch'))

    forced_launch_button = IOButton(3)
    forced_launch_button.disabled = True
    dialog.add(CONTROL_FRAME['x'] + 2,
               CONTROL_FRAME['y'] + 3, forced_launch_button)
    dialog.add(CONTROL_FRAME['x'] + 6,
               CONTROL_FRAME['y'] + 3, WLabel('Force launch'))

    force_gate_button = IOButton(3)
    dialog.add(CONTROL_FRAME['x'] + 2,
               CONTROL_FRAME['y'] + 5, force_gate_button)
    dialog.add(CONTROL_FRAME['x'] + 6,
               CONTROL_FRAME['y'] + 5, WLabel('Force open gate'))

    blockage_button = IOButton(3)
    dialog.add(CONTROL_FRAME['x'] + 2,
               CONTROL_FRAME['y'] + 7, blockage_button)
    dialog.add(CONTROL_FRAME['x'] + 6,
               CONTROL_FRAME['y'] + 7, WLabel('Override blockage'))
    # keycard
    keycard_button = KeyCardButton(KEYCARD_FRAME['width'] - 2)
    dialog.add(KEYCARD_FRAME['x'] + 1, KEYCARD_FRAME['y'] + 1, keycard_button)

    def update_ui():
        state.scan_state()
        announcement_label.redraw()
        update_launch_label()
        update_operation_label()
        update_mech_label()
        update_gate_label()
        update_launchpad_label()
        launch_request_button.state = state.launch_request
        launch_request_button.redraw()
        force_gate_button.state = state.force_open_gate
        force_gate_button.redraw()
        blockage_button.state = state.override_blockage
        blockage_button.redraw()
        keycard_button.state = state.key_card
        keycard_button.redraw()
        boot_mech()
    global update_ui_fn
    update_ui_fn = update_ui

    def handle_launch_request_button(_):
        state.launch_request = True
        update_ui()
        time.sleep(0.5)
        state.launch_request = False
        update_ui()
    launch_request_button.on("click", handle_launch_request_button)

    def handle_force_gate_button(_):
        state.force_open_gate = True
        update_ui()
        time.sleep(0.5)
        state.force_open_gate = False
        update_ui()
    force_gate_button.on("click", handle_force_gate_button)

    def handle_blockage_button(_):
        state.override_blockage = True
        update_ui()
        time.sleep(0.5)
        state.override_blockage = False
        update_ui()
    blockage_button.on("click", handle_blockage_button)

    def handle_key_card(_):
        state.key_card = not state.key_card
        update_ui()
    keycard_button.on("click", handle_key_card)

    return dialog


def main_loop():
    global dialog
    dialog.redraw()
    while True:
        global update_ui_fn
        update_ui_fn()
        key = dialog.get_input()
        if key is None:
            continue
        res = dialog.handle_input(key)

        if res is not None and res is not True:
            return res


try:
    with Context():
        dialog = create_dialog()

        screen_redraw(Screen)
        Screen.set_screen_redraw(screen_redraw)
        Screen.set_screen_resize(screen_resize)

        _ = main_loop()
except:
    pass
