# Solution
The provided diagram shows the internal workings of the system using ladder logic.

Launching the mech can be done in 2 ways. Both required the gate to be open.
- Manual mode: simply force the launch
- Automatic mode: the mech must be ready and the launch must be requested.

If we log in we see that both approaches do not work at first. The force launch button seems to be broken, blocking us from manually launching the mech. Also, it is not ready so the automatic approach won't work either. All we can do at this point is preparing other requirements: clearing the launch tube and opening the gate. Both must be done in manual mode. Once this is done, the mech will start booting by itself. When it is ready, we can simply switch back to automatic mode and request a launch.
