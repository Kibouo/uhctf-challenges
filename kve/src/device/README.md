# EX6100v1
- firmware: https://www.downloads.netgear.com/files/GDC/EX6100/EX6100-V1.0.2.28_1.1.138.zip
- CVE: https://nvd.nist.gov/vuln/detail/CVE-2022-24655
- PoC: https://github.com/doudoudedi/Netgear_product_stack_overflow
